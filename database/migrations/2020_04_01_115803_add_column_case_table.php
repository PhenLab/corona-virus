<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cases', function(Blueprint $table){
            $table->bigInteger('city_id')->after('nationality')->nullable();
            $table->bigInteger('province_id')->after('city_id')->nullable();
            $table->bigInteger('district_id')->after('province_id')->nullable();
            $table->bigInteger('commune_id')->after('district_id')->nullable();
            $table->bigInteger('village_id')->after('commune_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cases', function(Blueprint $table){
            $table->dropColumn('city_id');
            $table->dropColumn('province_id');
            $table->dropColumn('district_id');
            $table->dropColumn('commune_id');
            $table->dropColumn('village_id');
        });
    }
}
