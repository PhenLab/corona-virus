<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescColumnToNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('news', 'desc'))
        {
            Schema::table('news', function (Blueprint $table)
            {
                $table->dropColumn('desc');
            });

        }
        Schema::table('news', function (Blueprint $table) {
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        if (Schema::hasColumn('news', 'desc'))
        {
            Schema::table('news', function (Blueprint $table)
            {
                $table->dropColumn('desc');

            });

        }
        if (Schema::hasColumn('news', 'description'))
        {

            Schema::table('news', function (Blueprint $table)

            {

                $table->dropColumn('description');

            });

        }
    }
}
