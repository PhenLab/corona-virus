<?php

use Illuminate\Database\Seeder;
use App\User;
use App\City;
use Carbon\Carbon;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create_at = Carbon::now();
        $user = User::select('id')->first();

        $cities =  [
            [
                'name' => 'Phnom Penh',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Sihanoukville',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
        ];
        City::insert($cities);
    }
}
