<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Khan;
use App\City;
use Carbon\Carbon;

class KhanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $create_at = Carbon::now();
        $user = User::select('id')->first();
        $phnom_penh = City::getIDCity('Phnom Penh');
        $preah_sihanouk = City::getIDCity('Sihanoukville');

        // District of Phnom Penh
        $khan =  [
            [
                'name' => 'Chamkar Mon',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Doun Penh',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Prampir Makara',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Toul Kork',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Dangkor',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Mean Chey',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Russey Keo',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Sen Sok',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Pou Senchey',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Chroy Changvar',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Preaek Pnov',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Chbar Ampov',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Boeng Keng Kang',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kombol',
                'city_id' => $phnom_penh,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            // District of Sihanoukville
            [
                'name' => 'Preah Sihanouk',
                'city_id' => $preah_sihanouk,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Prey Nob',
                'city_id' => $preah_sihanouk,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Steung Hav',
                'city_id' => $preah_sihanouk,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kampong Seila',
                'city_id' => $preah_sihanouk,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],

          ];

        Khan::insert($khan);

    }
}
