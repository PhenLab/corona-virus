<?php

use Illuminate\Database\Seeder;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CityTableSeeder::class);
        $this->call(ProvinceTableSeeder::class);
        $this->call(KhanTableSeeder::class);
        $this->call(SangkatTableSeeder::class);
        $this->call(KrongSrokTableSeeder::class);
        $this->call(SangkatKhumTableSeeder::class);
    }
}
