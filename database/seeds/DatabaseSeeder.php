<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $admin = User::firstOrNew([
            'name' => 'Admin',
            'email' => 'admin@moh.com',
        ]);
        if(!$admin->id){
            $admin->password = Hash::make('admin123');
        }
        $admin->save();
    }
}
