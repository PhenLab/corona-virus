<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Province;
use Carbon\Carbon;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create_at = Carbon::now();
        $user = User::select('id')->first();

        $provinces =  [
            [
                'name' => 'Ratanakiri',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Battambang',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Svay Rieng',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kampong Speu',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Takeo',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kampong Cham',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kampong Chhnang',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kampot',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Koh Kong',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kratie',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Pailin',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Prey Veng',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Pursat',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Oddar Meanchey',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kep',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Mondulkiri',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Banteay Meanchey',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Siem Reap',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kampong Thom',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Stung Treng',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Tboung Khmum',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Kandal',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ],
            [
                'name' => 'Preah Vihear',
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => $create_at,
                'updated_at' => $create_at,
            ]
          ];

          Province::insert($provinces);
    }
}
