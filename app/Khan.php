<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Khan extends Model
{

    protected $fillable = [
        'name', 'khan_id','created_by', 'updated_by','created_at', 'updated_at',
    ];

    public static function getIDKhan($str){
        $khan = Khan::select('id')->where('name','=',$str)->first();
        $khan_id = $khan->id;
        return $khan_id;
    }

    public function creator()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function updater()
    {
        return $this->belongsTo('App\User','updated_by');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
