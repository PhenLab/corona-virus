<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    protected $fillable = [
        'name', 'created_by', 'updated_by','created_at', 'updated_at',
    ];

    public static function getIDCity($str){
        $city = City::select('id')->where('name','=',$str)->first();
        $city_id = $city->id;
        return $city_id;
    }

    public function creator()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function updater()
    {
        return $this->belongsTo('App\User','updated_by');
    }
    
}
