<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KrongSrok extends Model
{
    protected $fillable = [
        'name', 'province_id','created_by', 'updated_by','krong','created_at', 'updated_at',
    ];

    public static function getIDKrongSrok($str){
        $KrongSrok = KrongSrok::select('id')->where('name','=',$str)->first();
        $KrongSrok_id = $KrongSrok->id;
        return $KrongSrok_id;
    }

    public function creator()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function updater()
    {
        return $this->belongsTo('App\User','updated_by');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
    
}
