<?php
namespace App\Helper;
use Illuminate\Support\Facades\Route;

class Helper{

    public static $coordiate_full_lenght = 12;
    public static $digit_after_dot = 9;
    public static $currentActive = null;
    public const  POSITIVE = array(
        'id'=>'2',
        'value'=>'POSITIVE',
        'color'=>'badge badge-info',

    );
    public const SUSPECTED = array(
        'id'=>'1',
        'value'=>'SUSPECTED',
        'color'=>'badge badge-warning',

    );
    public const NEGATIVE = array(
        'id'=>'0',
        'value'=>'NEGATIVE',
        'color'=>'badge badge-primary',

    );
    public const RECOVERED = array(
        'id'=>'3',
        'value'=>'RECOVERED',
        'color'=>'badge badge-success',

    );
    public const DEAD = array(
        'id'=>'4',
        'value'=>'DEAD',
        'color'=>'badge badge-danger',

    );
    
    public static function getActiveMenu($routes){

        if (self::$currentActive == null)
            self::$currentActive = Route::currentRouteName();

        // self::$currentActive = str_replace('backend/','',self::$currentActive);    
        \Log::debug('check_route'. self::$currentActive);
        if (is_array($routes)){
            if (in_array(self::$currentActive,$routes))
                return ' active ';
        }else if (self::$currentActive ==  $routes)
            return ' active ';

        return '';
    }

    public static function roundCoordinate($lat_lng)
    {
        # code...
        if(strlen($lat_lng) - 1 > self::$coordiate_full_lenght){
            $l = explode('.',$lat_lng);
            $head_lenght  = strlen($l[0]);
            $round_num = self::$coordiate_full_lenght - $head_lenght;
            if($round_num > self::$digit_after_dot)
                $round_num = self::$digit_after_dot;

            $after_dot = substr($l[1],0,$round_num);    

            $lat_lng = $l[0].'.'.$after_dot;

        }
        return $lat_lng;
    }

}

?>