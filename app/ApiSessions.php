<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiSessions extends Model
{
    //
    public function user()
    {
        # code...
        return $this->belongsTo('App\User','user_id');
    }
}
