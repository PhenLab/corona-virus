<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [
        'name', 'created_by', 'updated_by','created_at', 'updated_at',
    ];

    public static function getIDProvince($str){
        $province = Province::select('id')->where('name','=',$str)->first();
        $province_id = $province->id;
        return $province_id;
    }
    
    public function creator()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function updater()
    {
        return $this->belongsTo('App\User','updated_by');
    }
}
