<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{
    //
    public function location()
    {
        # code...
        return $this->belongsTo('\App\Locations','location_id');
    }
}
