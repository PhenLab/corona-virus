<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SangkatKhum extends Model
{
    protected $fillable = [
        'name', 'krong_srok_id','created_by', 'updated_by','sangkat','created_at', 'updated_at',
    ];

    public static function getIDSangkatKhum($str){
        $khan = KrongSrok::select('id')->where('name','=',$str)->first();
        $khan_id = $khan->id;
        return $khan_id;
    }

    public function creator()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function updater()
    {
        return $this->belongsTo('App\User','updated_by');
    }

    public function krongsrok()
    {
        return $this->belongsTo('App\KrongSrok','krong_srok_id','id');
    }
}
