<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationTracking extends Model
{
    //
    public $timestamps = false;

    protected $table = 'tracking_locations';
    public function case()
    {
        # code...
        return $this->belongsTo('App\Cases','user_name','phone');
    }
}
