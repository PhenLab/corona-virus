<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helper\Helper;

class Locations extends Model
{
    //
    public function cases()
    {
        # code...
        return $this->hasMany('App\Cases','location_id','id');
    }
    public function all_dead()
    {
        # code...
        return $this->cases()->where('cases.status',Helper::DEAD['id']);
    }
    public function all_suspected()
    {
        # code...
        return $this->cases()->where('cases.status', Helper::SUSPECTED['id']);
    }
    public function all_recovered()
    {
        # code...
        return $this->cases()->where('cases.status',Helper::RECOVERED['id']);
    }
    public function all_positive()
    {
        # code...
        return $this->cases()->where('cases.status',Helper::POSITIVE['id']);
    }
    public function all_negative()
    {
        # code...
        return $this->cases()->where('cases.status',Helper::NEGATIVE['id']);
    }
    
}
