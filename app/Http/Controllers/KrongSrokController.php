<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Province;
use App\KrongSrok;

class KrongSrokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $krong_srok = KrongSrok::orderBy('province_id')->get();
        return view('krong_srok.index',compact('krong_srok')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Province::orderBy('name')->get(); 
        return view('krong_srok.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = $this->validate($request,[
            'name'=>'required|max:256',
        ]);
        dd($request->krong);
        $value = 0;
        if($request->krong != 1)
            $value = 1;

        $krongsrok = new KrongSrok();
        $krongsrok->name =  $request->name;
        $krongsrok->province_id =  $request->province;
        $krongsrok->created_by = Auth::id();
        $krongsrok->updated_by = Auth::id();
        $krongsrok->krong =  $value;
        $krongsrok->save();

        $this->alertMessage($request,1,true);
        return redirect(route('krong_srok.show',$krongsrok->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data  = KrongSrok::where('id',$id)->with('province','creator','updater')->first();
        return view('krong_srok.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = KrongSrok::where('id',$id)->first();
        $provinces = Province::orderBy('name')->get(); 
        return view('krong_srok.edit',compact('data','provinces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = $this->validate($request,[
            'name'=>'required'        
        ]);

        $value = 0;
        if($request->krong != 1)
            $value = 1;

        $krongsrok = KrongSrok::where('id',$id)->first();
        $krongsrok->name =  $request->name;
        $krongsrok->province_id =  $request->province;
        $krongsrok->updated_by = Auth::id();
        $krongsrok->krong =  $value;

        $krongsrok->save();
        $this->alertMessage($request,2,true);
        return redirect(route('krong_srok.show',$krongsrok->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        KrongSrok::where('id',$request->id)->delete();
        $this->alertMessage($request,3,true);
        return response()->json([
             'status'=>'success'
        ],200);
    }


    // public function delete(Request $request)
    // {
    //     $obj =  KrongSrok::where('id',$request->id)->delete();

    //     $this->alertMessage($request,3,true);
    //     return response()->json([
    //         'status'=>'success'
    //     ],200);
    // }
}
