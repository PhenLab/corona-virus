<?php

namespace App\Http\Controllers;

use App\Cases;
use App\LocationTracking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Client;
class UserLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datas = LocationTracking::all()->groupBy('user_name');

        return view('user-location.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
        $data = LocationTracking::where('user_name',$id)->orderBy('created_at','desc')->first();

        return view('user-location.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getUserLocation(Request $request)
    {
        # code...
        $user_name = $request->user_name;
        $obj = LocationTracking::where('user_name',$user_name)->orderBy('created_at','desc')->first();

        return $this->apiResponseSuccess($obj);
    }
    public function showRealTimeLocations(Request $request)
    {
        $datas = self::getAllUserLocations();

        $title = 'All User Realtime Locations';
        return view('user-location.realtime', compact('datas','title'));
        
    }
    public function getAllUserRealTimeLocation(Request $request)
    {
        $datas = self::getAllUserLocations();

        return $this->apiResponseSuccess($datas);
        
    }
    public function getAllUserLocations()
    {
        # code...
        $datas = DB::table('tracking_locations as tl')
            ->join(DB::raw("(SELECT MAX(created_at) as created_at,user_name
                FROM tracking_locations 
                GROUP BY user_name) ltl"), 
                [ ['tl.user_name', '=', 'ltl.user_name'],['tl.created_at', '=', 'ltl.created_at'] ])
            ->leftJoin('cases as c','c.phone','tl.user_name')
            ->select('tl.*','c.name')
            ->get();
        
        return $datas;
    }
    public function pushResquestRealTimeLocation(Request $request)
    {
        # code...
        $user_name = $request->user_name;
        $bot_url = config("app.bot_url");
        $bot_token = config("app.bot_token");
        $full_realtime_request_url = $bot_url.'/mohbot.php?token='.$bot_token.'&to='.$user_name.'&mode=2&msg=';

        // $re = exec('curl -L '.$full_realtime_request_url);
        //Once again, we use file_get_contents to GET the URL in question.
        $re = file_get_contents($full_realtime_request_url);
        
        $check = strpos($re,'ok');
        if($check != false && $check >= 0)
            return $this->apiResponseSuccess();

        return $this->apiResponseError();

    }
}
