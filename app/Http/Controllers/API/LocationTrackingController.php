<?php

namespace App\Http\Controllers\API;

use App\Cases;
use App\Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LocationTracking;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class LocationTrackingController extends Controller
{
   public function update_user_location(Request $request)
   {
        # code...
        $v = Validator::make($request->all(),[
            'user_name'=> 'required',
            'lon'=> 'required',
            'lat'=> 'required',
        ]);
        
        if($v->fails()){
                return $this->apiResponseError(422, $v->errors()->all());
        }
    
        $user_name = $request->user_name;
        $lon = $request->lon;
        $lat = $request->lat;

        $lat = Helper::roundCoordinate($lat);
        $lon = Helper::roundCoordinate($lon);
        $obj = LocationTracking::where('user_name',$user_name)->where('lat',$lat)->where('lon',$lon)->first();

        if(!$obj){
            $obj =  new LocationTracking();
            $obj->user_name = $user_name;
            $obj->lon = $lon;
            $obj->lat = $lat;
            $obj->created_at = Carbon::now();
            $obj->save();
        }
        return $this->apiResponseSuccess();
    

   }
}
