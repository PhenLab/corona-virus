<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\SangkatKhum;
use App\KrongSrok;
use App\Khan;
use App\Sangkat;

class SangkatKhumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sangkat_khum = SangkatKhum::orderBy('krong_srok_id')->get();
        return view('sangkat_khum.index',compact('sangkat_khum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = KrongSrok::orderBy('name')->get(); 
        return view('sangkat_khum.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = $this->validate($request,[
            'name'=>'required|max:256',
        ]);
        
        $sangkat = 0;
        if($request->khum != 1)
            $sangkat = 1;

        $sangkat_khum = new SangkatKhum();
        $sangkat_khum->name =  $request->name;
        $sangkat_khum->krong_srok_id =  $request->krongsrok;
        $sangkat_khum->created_by = Auth::id();
        $sangkat_khum->updated_by = Auth::id();
        $sangkat_khum->sangkat = $sangkat;
        $sangkat_khum->save();

        $this->alertMessage($request,1,true);
        return redirect(route('sangkat_khum.show',$sangkat_khum->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data  = SangkatKhum::where('id',$id)->with('creator','updater')->first();
        return view('sangkat_khum.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SangkatKhum::where('id',$id)->first();
        $krong_srok = KrongSrok::orderBy('name')->get(); 
        return view('sangkat_khum.edit',compact('data','krong_srok'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = $this->validate($request,[
            'name'=>'required|max:256',
        ]);
        
        $sangkat = 0;
        if($request->khum != 1)
            $sangkat = 1;

        $sangkat_khum = SangkatKhum::where('id',$id)->first();
        $sangkat_khum->name =  $request->name;
        $sangkat_khum->krong_srok_id =  $request->krongsrok;
        $sangkat_khum->updated_by = Auth::id();
        $sangkat_khum->sangkat = $sangkat;
        $sangkat_khum->save();

        $this->alertMessage($request,1,true);
        return redirect(route('sangkat_khum.show',$sangkat_khum->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    function destroy(Request $request)
    {
        SangkatKhum::where('id',$request->id)->delete();
        $this->alertMessage($request,3,true);
        return response()->json([
             'status'=>'success'
        ],200);
    }

    // public function delete(Request $request)
    // {
    //     $obj =  SangkatKhum::where('id',$request->id)->delete();

    //     $this->alertMessage($request,3,true);
    //     return response()->json([
    //         'status'=>'success'
    //     ],200);
    // }
}
