<?php

namespace App\Http\Controllers;

use App\Cases;
use App\Helper\Helper;
use App\Locations;
use App\City;
use App\Province;
use App\Khan;
use App\SangkatKhum;
use App\Sangkat;
use App\SangKhum;
use App\KrongSrok;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Location;
use DB;
use App\LocationTracking;
use Carbon\Carbon;

class CaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $obj = Cases::leftJoin('locations as l','l.id','cases.location_id')
                ->orderBy('cases.updated_at','desc')
                ->select('cases.*');
            
            
        // $datas = $obj->paginate(20);
        $datas = $obj->get();

        $title = 'Case List';
        return view('case.index',compact('datas','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $city = City::orderBy('name')->get();
        $province = Province::orderBy('name')->get(); 
        $title = 'Create Case';

        return view('case.create',compact('city','province','title'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $is_other_nation = $request->is_other_nation;
        $v = $this->validate($request,[
            'name'=>'required|max:256',
            'dob'=>'required',
            'nationality'=> Rule::requiredIf( $request->is_other_nation != 'true' ),
            'age'=>'required|numeric|min:0',
            'sex'=>'required',
            'other_nationality' => Rule::requiredIf( $request->is_other_nation == 'true' ),
            'location'=>'required',
            'phone'=>'nullable|numeric|unique:cases',
        ]);
        $name = $request->name;
        $dob = $request->dob;
        $status = $request->status;
        $age = $request->age;
        $sex = $request->sex;
        $note = $request->noteValue;
        $note = strip_tags($note);
        $location = $request->location;
        $city = $request->city;
        $province = $request->province;
        $district = $request->district;
        $commune = $request->commune;
        $is_create_new = $request->is_create_new;
        $nationality = $is_other_nation== 'true'? $request->other_nationality.'/is_other' : $request->nationality;
        $phone = $request->phone;

        $locations = explode('-',$location);
            
        
        $locations[0] = Helper::roundCoordinate($locations[0]);
        $locations[1] = Helper::roundCoordinate($locations[1]);



        $location = Locations::where(['lat'=>$locations[0],'lon'=>$locations[1] ])->first();
        if(! $location){

            $location = new Locations();
            $location->value = 1;
            $location->lat = $locations[0];
            $location->lon = $locations[1];
            $location->save();
        }
        else{
            $location->value = $location->value + 1;
            $location->save();
        }

        
        $obj = new Cases();
        $obj->name = $name;
        $obj->dob = $dob;
        $obj->nationality = $nationality;
        $obj->status = $status;
        $obj->age = $age;
        $obj->sex = $sex;
        $obj->note = $note;
        $obj->location_id = $location->id;
        $obj->city_id = $city;
        $obj->province_id = $province;
        $obj->district_id = $district;
        $obj->commune_id = $commune;
        $obj->phone = $phone;

        $obj->save();
        /**
         * initialize location of user base on phone number
         */

        if (isset($phone) && is_numeric($phone)) {
            # code...
            $location = new LocationTracking();
            $location->user_name = $phone;
            $location->lon = $locations[1];
            $location->lat = $locations[0];
            $location->created_at = Carbon::now();
            $location->save();

        }

        $this->alertMessage($request,1,true);

        if($is_create_new == 'true'){
            return redirect()->back()->withInput();
        }
        
        return redirect(route('case.show',$obj->id));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Cases::where('id',$id)->with('location')->first();
        $nationality = $data->nationality;
        if(strpos($nationality,'/is_other') >= 0){
            $is_other_nation = 'true';
            $nationality = explode('/',$nationality)[0];
        }
        if($data->city == 0){
            $c_p = City::where('id',$data->city_id)->pluck('name')->first();
            $district = Khan::where('id',$data->district_id)->pluck('name')->first();
            $commune = Sangkat::where('id',$data->district_id)->pluck('name')->first();
        }else{
            $c_p = Province::where('id',$data->province_id)->first();
            $district = KrongSrok::where('id',$data->district_id)->first();
            $commune = SangkatKhum::where('id',$data->district_id)->first();
        }

        $dataAddr = [
            "city" => $c_p,
            "district" => $district,
            "commune" => $commune,
        ];
        $title = 'Case Details';

        return view('case.show',compact('data','nationality','dataAddr','title'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Cases::where('id',$id)->with('location')->first();
        $nationality = $data->nationality;
        $is_other_nation = 'false';
        $other_nationality = '';
        if(strpos($nationality,'/is_other') != false && strpos($nationality,'/is_other') >= 0){
            $is_other_nation = 'true';
            $other_nationality = explode('/',$nationality)[0];
        }
        $city = City::orderBy('name')->get();
        $province = Province::orderBy('name')->get(); 
        $title = 'Update Case';

        return view('case.edit',compact('data','is_other_nation','other_nationality','city','province','title'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $is_other_nation = $request->is_other_nation;
        $v = $this->validate($request,[
            'name'=>'required|max:256',
            'dob'=>'required',
            'nationality'=> Rule::requiredIf( $request->is_other_nation != 'true' ),
            'age'=>'required|numeric|min:0',
            'sex'=>'required',
            'other_nationality' => Rule::requiredIf( $request->is_other_nation == 'true' ),
            'location'=>'required',
            'phone'=>'nullable|numeric|unique:cases,phone,'.$id

        ]);

        $name = $request->name;
        $dob = $request->dob;
        $status = $request->status;
        $age = $request->age;
        $sex = $request->sex;
        $note = $request->noteValue;
        $note = strip_tags($note);
        $location = $request->location;
        $city = $request->city;
        $province = $request->province;
        $district = $request->district;
        $commune = $request->commune;
        $phone = $request->phone;
        $nationality = $is_other_nation== 'true'? $request->other_nationality.'/is_other' : $request->nationality;
        
        $locations = explode('-',$location);
        $location = Locations::where(['lat'=>$locations[0],'lon'=>$locations[1] ])->first();
        $obj = Cases::where('id',$id)->first();

        if(! $location){

            //clear old location
            $old_location  = Locations::where('id',$obj->location_id)->first();
            if($old_location->value <= 1){
                Locations::where('id',$obj->location_id)->delete();
            }
            else{
                $old_location->value  =  $old_location->value - 1;
                $old_location->save();
            }

            $location = new Locations();
            $location->value = 1;
            $location->lat = $locations[0];
            $location->lon = $locations[1];
            $location->save();
            
        }
        else{
            $location->value = $location->value + 1;
            $location->save();
        }


        
        $obj->name = $name;
        $obj->dob = $dob;
        $obj->nationality = $nationality;
        $obj->city_id = $city;
        $obj->province_id = $province;
        $obj->district_id = $district;
        $obj->commune_id = $commune;
        $obj->status = $status;
        $obj->age = $age;
        $obj->sex = $sex;
        $obj->note = $note;
        $obj->location_id = $location->id;
        $obj->phone = $phone;
        $obj->save();



        $this->alertMessage($request,2,true);
        return redirect(route('case.show',$obj->id));





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Cases::where('id',$request->id)->delete();
        $this->alertMessage($request,3,true);
        return response()->json([
             'status'=>'success'
        ],200);
    }

    // public function delete(Request $request)
    // {
    //     //
        
    //     $obj =  Cases::where('id',$request->id)->delete();

    //     $this->alertMessage($request,3,true);
    //     return response()->json([
    //         'status'=>'success'
    //     ],200);


    // }

    public function districtsCity($id){
        $obj = Khan::where('city_id', $id)->get();
        return response()->json($obj);
    }

    public function districtsProvince($id){
        $obj = KrongSrok::where('province_id', $id)->get();
        return response()->json($obj);
    }
  
    public function commune($id){
        $obj = Sangkat::where('khan_id', $id)->get();
        return response()->json($obj);
    }

    public function communeProvince($id){
        $obj = SangkatKhum::where('krong_srok_id', $id)->get();
        return response()->json($obj);
    }

}
