<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use App\Khan;
use App\City;

class KhanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $khan = Khan::orderBy('city_id')->get();
        return view('khan.index',compact('khan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = City::orderBy('name')->get(); 
        return view('khan.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = $this->validate($request,[
            'name'=>'required|max:256',
        ]);
        
        $khan = new Khan();
        $khan->name =  $request->name;
        $khan->city_id =  $request->city;
        $khan->created_by = Auth::id();
        $khan->updated_by = Auth::id();
        $khan->save();

        $this->alertMessage($request,1,true);
        return redirect(route('khan.show',$khan->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data  = Khan::where('id',$id)->with('city','creator','updater')->first();
        return view('khan.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = khan::where('id',$id)->first();
        $cities = City::orderBy('name')->get(); 
        return view('khan.edit',compact('data','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = $this->validate($request,[
            'name'=>'required'        
        ]);

        $khan = Khan::where('id',$id)->first();
        $khan->name = $request->name;
        $khan->city_id = $request->city;
        $khan->updated_by = Auth::id();

        $khan->save();
        $this->alertMessage($request,2,true);
        return redirect(route('khan.show',$khan->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Khan::where('id',$request->id)->delete();
        $this->alertMessage($request,3,true);
        return response()->json([
             'status'=>'success'
        ],200);
    }

    // public function delete(Request $request)
    // {
    //     $obj =  Khan::where('id',$request->id)->delete();

    //     $this->alertMessage($request,3,true);
    //     return response()->json([
    //         'status'=>'success'
    //     ],200);
    // }
}
