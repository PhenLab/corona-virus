<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datas = News::orderBy('query_number')->with('creator')->get();
        $title = 'Message List';
        return view('news.index', compact('datas','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title = 'Create Message';

        return view('news.create',compact('title'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $v = $this->validate($request,[
        //     'query_number'=>'required|numeric|unique:news',
        //     'body'=>'required',
            
        // ]);
        // $obj = new News();

        // $obj->query_number = $request->query_number;
        // $obj->body = $request->body;
        // $obj->created_by = Auth::id();
        // $obj->updated_by = Auth::id();

        // $obj->save();

        // $this->alertMessage($request,1,true);

        // return redirect(route('news.show',$obj->id));

        $v = Validator::make($request->all(),[
            'query_number'=>'required|numeric|max:128|min:0|unique:news',
            'body'=>'required',
            // 'description'=>'required'
        ]);
        if($v->fails())
            return $this->apiResponseError(422,$v->errors()->all());

        $obj = new News();

        $obj->query_number = $request->query_number;
        //clear  link text
        $body = strip_tags($request->body);
        $obj->body = $body;
        $description = strip_tags($request->description);
        $obj->description = $description;

        $obj->created_by = Auth::id();
        $obj->updated_by = Auth::id();

        $obj->save();

        $this->alertMessage($request);
        return $this->apiResponseSuccess($obj);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data  = News::where('id',$id)->with('creator','updater')->first();
        $title = 'Message Details';

        return view('news.show',compact('data','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = News::where('id',$id)->first();
        $title = 'Update Message';

        return view('news.edit',compact('data','title'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $v = Validator::make($request->all(),[
            'query_number'=>'required|numeric|max:128|min:0|unique:news,query_number, '.$id,
            'body'=>'required',
            // 'description'=>'required'
        ]);
        if($v->fails())
            return $this->apiResponseError(422,$v->errors()->all());

        $obj = News::where('id',$id)->first();

        $obj->query_number = $request->query_number;
        //clear  link text
        $body = strip_tags($request->body);
        $obj->body = $body;
        $description = strip_tags($request->description);
        $obj->description = $description;
        $obj->updated_by = Auth::id();

        $obj->save();

        $this->alertMessage($request,2,true);

        return $this->apiResponseSuccess($obj);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        News::where('id',$request->id)->delete();
        $this->alertMessage($request,3,true);
        return response()->json([
             'status'=>'success'
        ],200);
    }
    // public function delete(Request $request)
    // {
    //     //
        
    //     $obj =  News::where('id',$request->id)->delete();

    //     $this->alertMessage($request,3,true);
    //     return response()->json([
    //         'status'=>'success'
    //     ],200);


    // }
}
