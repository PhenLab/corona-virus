<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Locations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PlotController extends Controller
{
    
    public function allCases( Request $request)
    {
        # code...
        //generat file contain cases 
        $status = $request->status;
        $folder = 'case';
        $file_name = 'data-for-plot.csv';
        if(! Storage::exists($folder)){
            Storage::makeDirectory($folder);
        }

        $storage_file_url = '/plot/file/'.$folder .'/'. $file_name;
        $content_value = 'Hrapx,Hrapy,Lat,Lon,Globvalue'."\n";
        $location = Locations::whereHas('cases', function ($query) use($status)
        {
            if(isset($status)){
                $query->where('cases.status', '=', $status);
            }
        })->get();
        foreach ($location as $key => $value) {
            # code...
            $all_cases = count($value->cases()->get());
            if($all_cases > 0){
                
                $content_value .= '0,';
                $content_value .= '0,';
                $content_value .= $value->lat.',';
                $content_value .= $value->lon.',';
            
                if(isset($status)){

                    if($status == Helper::DEAD['id']){
                        $all_dead =  count($value->all_dead()->get());
                        $content_value .= ' DEAD ('.$all_dead.')'."\n";

                    }
                    else if($status == Helper::POSITIVE['id']){
                        $all_positive =  count($value->all_positive()->get());
                        $content_value .= ' POSITIVE ('.$all_positive.')'."\n";

                    }
                    else if($status == Helper::NEGATIVE['id']){
                        $all_negative =  count($value->all_negative()->get());
                        $content_value .= ' NEGATIVE ('.$all_negative.')'."\n";

                    }
                    else if($status == Helper::SUSPECTED['id']){
                        $all_suspected =  count($value->all_suspected()->get());
                        $content_value .= ' SUSPECTED ('.$all_suspected.')'."\n";

                    }
                    else if($status == Helper::RECOVERED['id']){
                        $all_recovered = count($value->all_recovered()->get());
                        $content_value .= ' RECOVERED ('.$all_recovered.')'."\n";

                    }
                }else{
                    $all_dead =  count($value->all_dead()->get());

                    $all_suspected =  count($value->all_suspected()->get());

                    $all_positive =  count($value->all_positive()->get());

                    $all_negative =  count($value->all_negative()->get());

                    $all_recovered = count($value->all_recovered()->get());
                    $content_value .= 'Cases ('. $all_cases.') NEGATIVE ('.$all_negative.') SUSPECTED ('.$all_suspected.') POSITIVE ('.$all_positive.') RECOVERED ('.$all_recovered.') DEAD ('.$all_dead.')'."\n";

                }
                
            }
        }
        
        Storage::put($folder.'/'.$file_name, $content_value);

        $title = 'Corona Case Visualization';
        return view('plot.cases', compact('storage_file_url','status','title'));
    }
}
