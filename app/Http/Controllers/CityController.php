<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $city = City::orderBy('name')->get();
        return view('city.index',compact('city'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = $this->validate($request,[
            'name'=>'required|max:256',
        ]);
            
        $city = new City();
        $city->name =  $request->name;
        $city->created_by = Auth::id();
        $city->updated_by = Auth::id();
        $city->save();
        $this->alertMessage($request,1,true);
        return redirect(route('city.show',$city->id));     
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data  = City::where('id',$id)->with('creator','updater')->first();
        return view('city.show',compact('data'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = City::where('id',$id)->first();
        return view('city.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = $this->validate($request,[
            'name'=>'required'        
        ]);

        $city = City::where('id',$id)->first();
        $city->name = $request->name;
        $city->updated_by = Auth::id();

        $city->save();
        $this->alertMessage($request,2,true);
        return redirect(route('city.show',$city->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        City::where('id',$request->id)->delete();
        $this->alertMessage($request,3,true);
        return response()->json([
             'status'=>'success'
        ],200);
    }

    // public function delete(Request $request)
    // {
    //     $obj =  City::where('id',$request->id)->delete();

    //     $this->alertMessage($request,3,true);
    //     return response()->json([
    //         'status'=>'success'
    //     ],200);
    // }

}
