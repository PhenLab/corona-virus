<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sangkat;
use App\Khan;

class SangkatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sangkat = Sangkat::orderBy('khan_id')->get();
        return view('sangkat.index',compact('sangkat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Khan::orderBy('name')->get(); 
        return view('sangkat.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = $this->validate($request,[
            'name'=>'required|max:256',
        ]);
        
        $sangkat = new Sangkat();
        $sangkat->name =  $request->name;
        $sangkat->khan_id =  $request->khan;
        $sangkat->created_by = Auth::id();
        $sangkat->updated_by = Auth::id();
        $sangkat->save();

        $this->alertMessage($request,1,true);
        return redirect(route('sangkat.show',$sangkat->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data  = Sangkat::where('id',$id)->with('creator','updater')->first();
        return view('sangkat.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Sangkat::where('id',$id)->first();
        $cities = Khan::orderBy('name')->get(); 
        return view('sangkat.edit',compact('data','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = $this->validate($request,[
            'name'=>'required'        
        ]);

        $khan = Sangkat::where('id',$id)->first();
        $khan->name = $request->name;
        $khan->khan_id = $request->khan;
        $khan->updated_by = Auth::id();

        $khan->save();
        $this->alertMessage($request,2,true);
        return redirect(route('sangkat.show',$khan->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Sangkat::where('id',$request->id)->delete();
        $this->alertMessage($request,3,true);
        return response()->json([
             'status'=>'success'
        ],200);
    }

    // public function delete(Request $request)
    // {
    //     $obj =  Sangkat::where('id',$request->id)->delete();

    //     $this->alertMessage($request,3,true);
    //     return response()->json([
    //         'status'=>'success'
    //     ],200);
    // }
}
