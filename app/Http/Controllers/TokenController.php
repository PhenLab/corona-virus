<?php

namespace App\Http\Controllers;

use App\ApiSessions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class TokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datas = ApiSessions::with('user')->get();
        
        return view('token.index', compact('datas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('token.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $v = $this->validate($request,[
            'desc'=>'required',
            
        ]);
        $user = Auth::user();


        if($user->api_token){
        
            $this->alertMessage($request,1,false,'Token created');
        }
        else{
            $token = Str::random(60);

            $request->user()->forceFill([
                'api_token' => hash('sha256', $token),
            ])->save();


            $obj = new ApiSessions();

            $obj->desc = $request->desc;
            $obj->user_id = Auth::id();

            $obj->save();

            $this->alertMessage($request,1,true);
        }

        return redirect(route('token.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ApiSessions::where('id',$id)->first();
        return view('token.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $v = $this->validate($request,[
            'desc'=>'required',
            
        ]);
        
        //
        if($request->refresh == 'on'){
            $token = Str::random(60);
            $request->user()->forceFill([
                'api_token' => hash('sha256', $token),
            ])->save();
        }
        


        $obj = ApiSessions::where('id',$id)->first();

        $obj->desc = $request->desc;
        $obj->user_id = Auth::id();

        $obj->save();

        $this->alertMessage($request,2,true);

        return redirect(route('token.index',$obj->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
