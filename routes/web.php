<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login','Auth\LoginController@login');

Route::post('logout','Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function(){
    Route::get('/', function () {
        return redirect('case');
    });
    // Route::get('/dashboard', function () {
    //     return view('dashboard');
    // });
    Route::resource('case','CaseController');
    Route::get('/query-city/{id}','CaseController@districtsCity');
    Route::get('/query-province/{id}','CaseController@districtsProvince');
    Route::get('/query-district/{id}','CaseController@commune');
    Route::get('/query-districtProvince/{id}','CaseController@communeProvince');

    Route::resource('news','NewsController');

    Route::resource('token','TokenController');
    
    Route::get('user-location/realtime','UserLocationController@showRealTimeLocations')->name('user.realtime.locations');
    Route::post('user-location/request-realtime','UserLocationController@pushResquestRealTimeLocation');

    Route::resource('user-location','UserLocationController');
    Route::post('user-location/get','UserLocationController@getUserLocation');
    Route::post('user-location/get-all','UserLocationController@getAllUserRealTimeLocation');

    
    Route::get('plot/all-cases','PlotController@allCases');

    Route::resource('city','CityController');

    Route::resource('province','ProvinceController');

    Route::resource('khan','KhanController');

    Route::resource('krong_srok','KrongSrokController');

    Route::resource('sangkat','SangkatController');
    
    Route::resource('sangkat_khum','SangkatKhumController');
    
    Route::get('plot/file/{folder_name}/{file_name}',function ($folder_name,$file_name){
        $path = storage_path() . '/app/' . $folder_name . '/'  . $file_name;
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    });

    Route::get('/home', function ()
    {
        return redirect('case');
    })->name('home');
});