@extends('layout.master')
@section('style')
  @include('partials.css-plugin-for-table')
  @include('partials.css-plugin-for-toastr')

@endsection
@section('content')
    <!-- Begin Page Content -->
    
    <div class="container-fluid">
        
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h5 mb-0 text-gray-800">Token List</h1>
          <div >

            <a href="{{route('token.create')}}" class="btn btn-sm btn-primary "><i class="fas fa-add"></i> Create New</a>

          </div>
        </div>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <td >&numero;</td>
                    <th >Description</th>
                    <th>API Key</th>
                    <th>User</th>
                    <th style="width:185px">Created At</th>

                    <th style="text-align:center; width:100px">Actions</th>

                  </tr>
                </thead>
                
                <tbody>
                  @php
                      $page = 0;
                      if(Request::get('page'))
                        $page  = Request::get('page')-1;
                      
                      $index = $page*20 + 1;
                  @endphp
                  @foreach (@$datas as $key => $item)
                    <tr>
                      <td>{{$key + $index}}</td>
                      <td style="word-break:break-all !important">{{$item->desc}}</td>
                      <td style="word-break:break-all !important">{{$item->user->api_token}}</td>
                      <td>{{$item->user->name}}</td>

                      <td>{{$item->created_at}}</td>

                      <td align="center">
                        <a href="#" onclick="copyTextToClipboard('{{$item->user->api_token}}')" data-toggle="tooltips" title="Copy Token">
                          <i class="fas fa-copy"></i>
                        </a>
                      </td>

                    </tr>
                  @endforeach
                  
                </tbody>
              </table>
            </div>
            {{-- {{@$datas->appends(request()->except('page'))->links()}} --}}
          </div>
        </div>

    </div>
      <!-- /.container-fluid -->
@endsection


@section('script')
  @include('partials.js-plugin-for-table')
  @include('partials.js-plugin-for-toastr')

@endsection