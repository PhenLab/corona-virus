@extends('layout.master')
@section('style')

@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 mb-0 text-gray-800">Create Tokens</h1>
        <a href="{{route('news.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>
      </div>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Case</h6>
        </div> --}}
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class=" col-md-12">
              <form class="form-horizontal" method="POST" action="{{route('token.store')}}">
                @csrf

                
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text" for="desc">Description</label>
                  <div class="col-sm-10">    
                    <textarea name="desc" id="desc" class="form-control"  rows="3">{{old('desc')}}</textarea>
                    <span class="text-danger">{{ $errors->first('desc') }}</span>    
                  </div>
                </div>
                <div class="form-group " style="text-align:end; padding-top:10px">
                    
                  <button type="submit" class="btn btn-primary btn-sm pull-right"> Save </button>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            {{-- <div class="modal-header">
              <h5 class="modal-title">Pick Any Location</h5>

              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div> --}}
            <div class="modal-body">
              <div id="map"></div>
            </div>
            <div class="modal-footer">
              {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Pick</button> --}}
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="confirmPosition">Pick</button>

            </div>
          </div>
        </div>
      </div>
@endsection

@section('script')

@endsection