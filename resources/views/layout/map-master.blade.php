<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MOH {{@$title?'| '.@$title:''}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!-- plugin for customize style  -->
    <link rel="stylesheet" href="{{asset('css/customizedStyle.css')}}">

    @yield('style')
  <style>
    .container{
      padding-top:15px;
      padding-bottom:15px;
      padding-right: 6px;
      padding-left: 6px;
      width: 100%;
    }
  </style>
</head>
<body>
    
    @yield('content')
    <script src="{{asset('template/vendor/jquery/jquery.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script>
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });


    </script>
    @yield('script')
</body>
</html>