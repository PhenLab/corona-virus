<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('/')}}">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Corona Visualization
      {{-- <sup>2</sup> --}}
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  
  <!-- Nav Item - Dashboard -->
  {{-- <li class="nav-item {{App\Helper\Helper::getActiveMenu('dashboard')}}">
    <a class="nav-link" href="{{url('dashboard')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li> --}}

  <?php
      $case = array(
          'case.index',
          'case.create',
          'case.edit',
          'case.show',
      );
  ?>
  <!-- Nav Item - Dashboard -->
  <li class="nav-item {{App\Helper\Helper::getActiveMenu($case)}}">
    <a class="nav-link" href="{{route('case.index')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Cases</span></a>
  </li>

  <?php
      $case = array(
          'news.index',
          'news.create',
          'news.edit',
          'news.show',
      );
  ?>
  <!-- Nav Item - Dashboard -->
  <li class="nav-item {{App\Helper\Helper::getActiveMenu($case)}}">
    <a class="nav-link" href="{{route('news.index')}}">
      <i class="fa fa-newspaper"></i>
      <span>Bot Messages</span></a>
  </li>

  <?php
      $user_location = array(
          'user-location.index',
          'user-location.show',

      );
  ?>
  <!-- Nav Item - Dashboard -->
  <li class="nav-item {{App\Helper\Helper::getActiveMenu($user_location)}}">
    <a class="nav-link" href="{{route('user-location.index')}}">
      <i class="fas fa-street-view"></i>
      <span>User Location</span></a>
  </li>

  <?php
    $city = array(
      'city.index',
      'city.create',
      'city.edit',
      'city.show',
    );
    $khan = array(
      'khan.index',
      'khan.create',
      'khan.edit',
      'khan.show',
    );
    $sangkat = array(
      'sangkat.index',
      'sangkat.create',
      'sangkat.edit',
      'sangkat.show',
    );
    $city_collection = array_merge($city,$khan,$sangkat)

  ?>
  <li class="nav-item {{App\Helper\Helper::getActiveMenu($city_collection)}}">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePagesCities" aria-expanded="true" aria-controls="collapsePagesCities">
      <i class="fas fa-fw fa-map-pin"></i>
      <span>ទីក្រុង</span>
    </a>
    <div id="collapsePagesCities" class="collapse 
    {{App\Helper\Helper::getActiveMenu($city) == ' active '?'show':''}} || 
    {{App\Helper\Helper::getActiveMenu($khan) == ' active '?'show':''}} ||
    {{App\Helper\Helper::getActiveMenu($sangkat) == ' active '?'show':''}} ||" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item {{App\Helper\Helper::getActiveMenu($city)}}" href="{{route('city.index')}}">ទីក្រុង</a>
        <a class="collapse-item {{App\Helper\Helper::getActiveMenu($khan)}}" href="{{ route('khan.index') }}">ខ័ណ្ឌ</a>
        <a class="collapse-item {{App\Helper\Helper::getActiveMenu($sangkat)}}" href="{{ route('sangkat.index') }}">សង្កាត់</a>
      </div>
    </div>
  </li>
  <?php
    $province = array(
      'province.index',
      'province.create',
      'province.edit',
      'province.show',
    );
    $krong_srok = array(
      'krong_srok.index',
      'krong_srok.create',
      'krong_srok.edit',
      'krong_srok.show',
    );
    $sangkat_khum = array(
      'sangkat_khum.index',
      'sangkat_khum.create',
      'sangkat_khum.edit',
      'sangkat_khum.show',
    );
    $province_collection = array_merge($province,$krong_srok,$sangkat_khum)
  ?>
  <li class="nav-item {{App\Helper\Helper::getActiveMenu($province_collection)}}">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePagesProvince" aria-expanded="true" aria-controls="collapsePagesCities">
      <i class="fas fa-fw fa-map-pin"></i>
      <span>ខេត្ត</span>
    </a>
    <div id="collapsePagesProvince" class="collapse 
    {{App\Helper\Helper::getActiveMenu($province) == ' active '?'show':''}} || 
    {{App\Helper\Helper::getActiveMenu($krong_srok) == ' active '?'show':''}} ||
    {{App\Helper\Helper::getActiveMenu($sangkat_khum) == ' active '?'show':''}} ||" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item {{App\Helper\Helper::getActiveMenu($province)}}" href="{{route('province.index')}}">ខេត្ត</a>
        <a class="collapse-item {{App\Helper\Helper::getActiveMenu($krong_srok)}}" href="{{ route('krong_srok.index') }}">ក្រុង​ និង ស្រុក</a>
        <a class="collapse-item {{App\Helper\Helper::getActiveMenu($sangkat_khum)}}" href="{{ route('sangkat_khum.index') }}">សង្កាត់ និង​ ឃុំ</a>
      </div>
    </div>
  </li>
  <!-- Nav Item - Pages Collapse Menu -->
  <?php
      $token = array(
          'token.index',
          'token.create',
      );
      $securities = array_merge($token);
  ?>
  <li class="nav-item {{App\Helper\Helper::getActiveMenu($securities)}}">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
      <i class="fas fa-fw fa-lock"></i>
      <span>Securities</span>
    </a>
    <div id="collapsePages" class="collapse  {{App\Helper\Helper::getActiveMenu($securities) == ' active '?'show':''}}" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item {{App\Helper\Helper::getActiveMenu($token)}}" href="{{route('token.index')}}">Tokens</a>

      </div>
    </div>
  </li>


</ul>
<!-- End of Sidebar -->