@extends('layout.master')
@section('style')
  @include('partials.css-plugin-for-toastr')

    <link rel="stylesheet" href="{{asset('plugins/select_country/css/countrySelect.css')}}">

    <style>
       #map {
        height: 500px;
      }
      
    </style>
@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 mb-0 text-gray-800"> Real Time User Location </h1>
        <div>
          <a href="#" onclick="requestUserCurrentLocation()" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-add"></i> Request User Current Location</a>

          <a href="{{route('user-location.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>

        </div>
      </div>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Case</h6>
        </div> --}}
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div id="map"></div>

          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

@endsection

@section('script')
    @include('partials.js-plugin-for-toastr')

    <script>


      var user_name = '{{$data->user_name}}'
      var user_location_lat = '{{$data->lat}}'
      var user_location_lng = '{{$data->lon}}'
      var real_time_location_interval = '{{config("app.real_time_location_interval")}}'
      var url_for_real_time_location = '/user-location/get'
      user_location_lat = Number(user_location_lat)
      user_location_lng = Number(user_location_lng)
      var current_location = {lat: user_location_lat, lng: user_location_lng}

      var realtime_location_request_url = '/user-location/request-realtime'

      var mapZoomLevel = 16;
      var map;
      var markers = [];
      var image = window.location.origin+ '/images/user-on-map.svg';


      function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
          zoom: mapZoomLevel,
          center: current_location,
          mapTypeId: 'terrain'
        });

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
          addMarker(event.latLng);
        });

        // Adds a marker at the center of the map.
        addMarker(current_location);
      }

      // Adds a marker to the map and push to the array.
      function addMarker(location) {

        map.panTo(new google.maps.LatLng(location.lat, location.lng));
        var marker = new google.maps.Marker({
          position: location,
          map: map,
          icon: image

        });
        markers.push(marker);
      }

      // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      setInterval(() => {

        $.ajax({
           type: 'post',
           url : url_for_real_time_location,
           data:{user_name: user_name},
           success: function(r){
               var lat = r.data.lat;
               var lng = r.data.lon;
               lat = Number(lat)
               lng = Number(lng)
               current_location.lat = lat;
               current_location.lng = lng;
               clearMarkers();
               addMarker(current_location);
            
           },
           error: function(e) {
               console.log(e);
           }
       })
        
      }, real_time_location_interval );

      function XHRequest(method = 'GET', url = '', async = false, token = '', callBackFunction) {
          var xhr = new XMLHttpRequest();
          xhr.open(method, url , async); // your url will pass to open method

          
          // headers.append('Access-Control-Allow-Credentials', 'true');

          xhr.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                  var data = JSON.parse(this.response);
                  if (typeof callBackFunction === 'function') callBackFunction(data);
              }
          };
          xhr.send();
      }

      function requestUserCurrentLocation(){
        
        $.ajax({
           type: 'post',
           url : realtime_location_request_url,
           data:{user_name: user_name},

           success: function(r){
              if(r.status === 'success')
                toastr["success"]("Request user realtime location success!") 
              else
                toastr["error"]("Request user realtime location error!")            
             

           },
           error: function(e) {
               console.log(e);

           }
       })
      }
    </script>
    @include('partials.script-for-realtime-location');
@endsection