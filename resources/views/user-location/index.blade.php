@extends('layout.master')
@section('style')
  @include('partials.css-plugin-for-table')

@endsection
@section('content')
    <!-- Begin Page Content -->
    
    <div class="container-fluid">
        
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h5 mb-0 text-gray-800">User Location List</h1>
          <div >

            <a href="{{route('user.realtime.locations')}}" class="btn btn-sm btn-primary "><i class="fas fa-add"></i> View Real Time Location on Map</a>

          </div>
        </div>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <td >&numero;</td>
                    <th >Phone</th>
                    <th >User Name</th>
                    <th >Created At</th>

                    <th style="text-align:center; width:100px">Actions</th>

                  </tr>
                </thead>
                
                <tbody>
                  @php
                      $page = 0;
                      if(Request::get('page'))
                        $page  = Request::get('page')-1;
                      
                      $index = $page*20 + 1;
                      $key = 0
                  @endphp
                  @foreach (@$datas  as $k => $item)
                    @php
                        $item = $item[sizeof($item) - 1];
                    @endphp
                    <tr>
                      <td>{{$key + $index}}</td>
                      <td >{{$item->user_name}}</td>
                      <td >{{@$item->case->name??'Unknown'}}</td>

                      <td>{{$item->created_at}}</td>

                      <td align="center">
                        
                        <a href="{{route('user-location.show', $k)}}">
                          <i class="fas fa-street-view"></i>
                        </a>
                      </td>

                    </tr>
                    @php
                        $key++;
                    @endphp
                  @endforeach
                  
                </tbody>
              </table>
            </div>
            {{-- {{@$datas->appends(request()->except('page'))->links()}} --}}
          </div>
        </div>

    </div>
      <!-- /.container-fluid -->
@endsection

@section('script')
    @include('partials.js-plugin-for-table')
@endsection