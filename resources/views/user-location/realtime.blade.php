@extends('layout.map-master')
@section('style')
    
  <style>
    .container{
      padding-top:15px;
      padding-bottom:15px;
      padding-right: 6px;
      padding-left: 6px;
      width: 100%;
    }
    #map {
        height: 700px;
      }
      
  </style>
@endsection
@section('content')

  <div class="container" >

    <div class="col-md-12" style="padding-left:0;padding-right:0">
      <form action="{{url('plot/all-cases')}}" method="get">
        {{-- <div class="col-md-10 col-sm-8 col-xs-6" style="padding-left:0">
          <select name="status" class="form-control bt-sm" id="status">
            <option value=""> ALL STATUS </option>
            <option value="{{App\Helper\Helper::POSITIVE['id']}}"  {{@$status ==App\Helper\Helper::POSITIVE['id']?'selected':''}}>  {{App\Helper\Helper::POSITIVE['value']}}</option>
            <option value="{{App\Helper\Helper::SUSPECTED['id']}}" {{@$status ==App\Helper\Helper::SUSPECTED['id']?'selected':''}}> {{App\Helper\Helper::SUSPECTED['value']}}</option>
            <option value="{{App\Helper\Helper::NEGATIVE['id']}}"  {{@$status ==App\Helper\Helper::NEGATIVE['id']?'selected':''}}>  {{App\Helper\Helper::NEGATIVE['value']}}</option>
            <option value="{{App\Helper\Helper::RECOVERED['id']}}" {{@$status ==App\Helper\Helper::RECOVERED['id']?'selected':''}}> {{App\Helper\Helper::RECOVERED['value']}}</option>
            <option value="{{App\Helper\Helper::DEAD['id']}}"      {{@$status ==App\Helper\Helper::DEAD['id']?'selected':''}}>      {{App\Helper\Helper::DEAD['value']}}</option>
          </select>
        </div> --}}
        <div class="col-md-12 col-sm-12 col-xs-12" style="padding-right:0; padding-left:0">
          {{-- <button type="submit" class="btn btn-primary bt-sm">SEARCH</button> --}}
          <a href="{{route('user-location.index')}}" class="btn btn-danger bt-sm pull-right"> Back To List</a>
        </div>

      </form>
    </div>


  </div>
  <div class="container" style="padding-top:0">
    <div class="col-md-12" style="padding-left:0;padding-right:0; ">

      <div id="map"></div>
    </div>
  </div>
  



@endsection

@section('script')
<script>

  var real_time_location_interval = '{{config("app.real_time_location_interval")}}'
  var url_for_real_time_location = '/user-location/get-all'
  
  var user_locations = {!! json_encode($datas->toArray()) !!};



  var cambodia_location = {lat: 12.486630529507185, lng:104.90747149601845};

  var mapZoomLevel = 8;
  var map;
  var markers = [];
  var image = window.location.origin+ '/images/users-on-map.svg';
  var list_of_open_info_window = [];
  var infoWindowList = [];


  function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
      zoom: mapZoomLevel,
      center: cambodia_location,
      // mapTypeId: 'terrain'
    });

    // This event listener will call addMarker() when the map is clicked.
    // map.addListener('click', function(event) {
    //   addMarker(event.latLng);
    // });

    // Adds a marker at the center of the map.
    addMarker(user_locations);
  }

  // Adds a marker to the map and push to the array.
  function addMarker(user_locations) {

    user_locations = Object.entries(user_locations)
    user_locations.forEach(element => {
      var last_location = element[1];
      var user_name = 'Unknown'
      var location = {lat: Number(last_location.lat), lng: Number(last_location.lon)}
      var marker = new google.maps.Marker({
        position: location,
        map: map,
        icon: image

      });
      markers.push(marker);
      if(last_location.name != null)
        user_name = last_location.name;


      var contentString = '<h5>'+user_name+'</h5>'+'<p>'+last_location.user_name+'</p>';
      var infoWindow = new google.maps.InfoWindow({
        content: contentString
      });

      infoWindowList.push(infoWindow)
      //event when click to show info window of marker 
      marker.addListener('click', function() {
          list_of_open_info_window = [];
          for (let index = 0; index < infoWindowList.length; index++) {
            const element = infoWindowList[index];
            element.close(res => {})
          }
            
          infoWindow.open(map, marker);
          var phone = $($(infoWindow.content)[1]).text()
          list_of_open_info_window.push(phone)

      });

      //event when close infor window of marker 
      google.maps.event.addListener(infoWindow,'closeclick',function(){
        var phone = $($(infoWindow.content)[1]).text()
        const index = list_of_open_info_window.indexOf(phone);
        if (index > -1) {
          list_of_open_info_window.splice(index, 1);
        }

      });

      //restore info window of marker
      var index_restore = list_of_open_info_window.indexOf(last_location.user_name)
      if(index_restore > -1){
        infoWindow.open(map, marker);
      }

    });
    
  }
  // Sets the map on all markers in the array.
  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkers() {
    setMapOnAll(null);
  }

  setInterval(() => {

    $.ajax({
      type: 'post',
      url : url_for_real_time_location,
      success: function(r){
          clearMarkers();
          addMarker(r.data);
        
      },
      error: function(e) {
          console.log(e);
      }
    })

  }, real_time_location_interval );

</script>
@include('partials.script-for-realtime-location');

@endsection