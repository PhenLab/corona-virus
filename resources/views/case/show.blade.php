@extends('layout.master')
@section('style')
    @include('partials.emoji.script-for-emoji')
    @include('partials.emoji.css-for-twemoji')
    
@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 mb-0 text-gray-800"> Case Details </h1>
        <div>
          <a href="{{route('case.edit',@$data->id)}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-add"></i> Update</a>
          <a href="{{route('case.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>

        </div>
      </div>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Case</h6>
        </div> --}}
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Name</b></label>
              <div class="col-sm-10">
                <p>{{$data->name}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Phone</b></label>
              <div class="col-sm-10">
                <p>{{$data->phone}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Date of Birth</b></label>
              <div class="col-sm-10">
                <p>{{$data->dob}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Age</b></label>
              <div class="col-sm-10">
                <p>{{$data->age}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Sex</b></label>
              <div class="col-sm-10">
                <p>{{$data->sex}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Nationality</b></label>
              <div class="col-sm-10">
                <p>{{$nationality}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Address</b></label>
              <div class="col-sm-10">
                <p>City or Province {{$dataAddr['city']}}, District {{$dataAddr['district']}}, Commune {{$dataAddr['commune']}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Note</b></label>
              <div class="col-sm-10">
                <p id="note" style="white-space: pre-wrap; line-height: 1;">{!! nl2br($data->note) !!}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Status</b></label>
              <div class="col-sm-10">
                @if ($data->status == App\Helper\Helper::DEAD['id'])
                <span class="{{App\Helper\Helper::DEAD['color']}}">{{App\Helper\Helper::DEAD['value']}}</span>

                @elseif($data->status == App\Helper\Helper::SUSPECTED['id'])
                  <span class="{{App\Helper\Helper::SUSPECTED['color']}}">{{App\Helper\Helper::SUSPECTED['value']}}</span>

                @elseif($data->status == App\Helper\Helper::RECOVERED['id'])
                  <span class="{{App\Helper\Helper::RECOVERED['color']}}">{{App\Helper\Helper::RECOVERED['value']}}</span>

                @elseif($data->status == App\Helper\Helper::POSITIVE['id'])
                  <span class="{{App\Helper\Helper::POSITIVE['color']}}">{{App\Helper\Helper::POSITIVE['value']}}</span>

                @elseif($data->status == App\Helper\Helper::NEGATIVE['id'])
                  <span class="{{App\Helper\Helper::NEGATIVE['color']}}">{{App\Helper\Helper::NEGATIVE['value']}}</span>
                @endif  
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Created At</b></label>
              <div class="col-sm-10">
                <p>{{$data->created_at}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Updated At</b></label>
              <div class="col-sm-10">
                <p>{{$data->updated_at}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Location</b></label>
              <div class="col-sm-10">
                <p>{{$data->location->lat.' - '.$data->location->lon}}</p>

                <div id="map"></div>
              </div>
            </div>

            {{-- <dl class = "dl-horizontal">
             
              <dt>Name</dt>
              <dd>{{$data->name}}</dd>
              <br/><br/>
              <dt>Phone</dt>
              <dd>{{$data->phone}}<br></dd>
              <br/><br/>
              <dt>DOB</dt>
              <dd>{{$data->dob}}<br></dd>
              <br/><br/>
              <dt>Age</dt>
              <dd >{{$data->age}}<br></dd>
              <br/><br/>
              <dt>Sex</dt>
              <dd>{{$data->sex}}<br></dd>
              <br/><br/>
              <dt>Nationality</dt>
              <dd>{{$nationality}}<br></dd>
              <br/><br/>
              <dt>Address</dt>
              <dd>City or Province {{$dataAddr['city']}}, District {{$dataAddr['district']}}, Commune {{$dataAddr['commune']}}<br></dd>
              <br/><br/>
              <dt>Note</dt>
              <dd id="note" style="white-space: pre-wrap; line-height: 1;">{!! nl2br($data->note) !!}<br></dd>
              <br/><br/>
              <dt>Status</dt>
              <dd>
                
                @if ($data->status == App\Helper\Helper::DEAD['id'])
                  <span class="{{App\Helper\Helper::DEAD['color']}}">{{App\Helper\Helper::DEAD['value']}}</span>

                @elseif($data->status == App\Helper\Helper::SUSPECTED['id'])
                  <span class="{{App\Helper\Helper::SUSPECTED['color']}}">{{App\Helper\Helper::SUSPECTED['value']}}</span>

                @elseif($data->status == App\Helper\Helper::RECOVERED['id'])
                  <span class="{{App\Helper\Helper::RECOVERED['color']}}">{{App\Helper\Helper::RECOVERED['value']}}</span>

                @elseif($data->status == App\Helper\Helper::POSITIVE['id'])
                  <span class="{{App\Helper\Helper::POSITIVE['color']}}">{{App\Helper\Helper::POSITIVE['value']}}</span>

                @elseif($data->status == App\Helper\Helper::NEGATIVE['id'])
                  <span class="{{App\Helper\Helper::NEGATIVE['color']}}">{{App\Helper\Helper::NEGATIVE['value']}}</span>
                @endif  
              <br></dd>
              <br/><br/>
              <dt>Created At</dt>
              <dd>{{$data->created_at}}<br></dd>
              <br/><br/>
              <dt>Updated At</dt>
              <dd>{{$data->updated_at}}<br/></dd>
              <br/><br/>
              <dt>Location</dt>
              <dd>{{$data->location->lat.' - '.$data->location->lon}}<br/></dd>

            </dl> --}}
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

@endsection

@section('script')
<script>
  var note = $('#note')
  $(note).html(twemoji.parse($(note).html()))
</script>

<script>

  var location_lat = '{{$data->location->lat}}'
  var location_lng = '{{$data->location->lon}}'
  location_lat = Number(location_lat)
  location_lng = Number(location_lng)

  var current_location = {lat: location_lat, lng: location_lng}
  var mapZoomLevel = 16

  var image = window.location.origin+ '/images/user-on-map.svg';


  function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
      zoom: mapZoomLevel,
      center: current_location,
      mapTypeId: 'terrain'
    });


    // Adds a marker at the center of the map.
    addMarker(current_location);
  }

  // Adds a marker to the map and push to the array.
  function addMarker(location) {

    map.panTo(new google.maps.LatLng(location.lat, location.lng));
    var marker = new google.maps.Marker({
      position: location,
      map: map,
      icon: image

    });
  }

</script>
@include('partials.script-for-realtime-location');

@endsection