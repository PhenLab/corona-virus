@extends('layout.master')
@section('style')
  @include('partials.css-plugin-for-table')

@endsection
@section('content')
    <!-- Begin Page Content -->
    
    <div class="container-fluid">
        
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h5 mb-0 text-gray-800">Case List</h1>
          <div >
            <a href="{{url('/plot/all-cases')}}" class=" btn btn-sm btn-primary "><i class="fas fa-add"></i> View Cases On Map</a>

            <a href="{{route('case.create')}}" class="btn btn-sm btn-primary "><i class="fas fa-add"></i> Create New</a>

          </div>
        </div>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          {{-- <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
          </div> --}}
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <td >&numero;</td>
                    <th style="width:12%">Name</th>
                    <th>Phone</th>
                    <th>DOB</th>
                    <th>Age</th>
                    <th>Sex</th>
                    <th>Nationality</th>
                    <th>Note</th>
                    <th style="text-align:center;">Status</th>
                    <th style="width:185px">Created At</th>
                    <th style="text-align:center; width:100px">Actions</th>

                  </tr>
                </thead>
                
                <tbody>
                  @php
                      $page = 0;
                      if(Request::get('page'))
                        $page  = Request::get('page')-1;
                      
                      $index = $page*20 + 1;
                  @endphp
                  @foreach ($datas as $key => $item)
                    <tr>
                      <td>{{$key + $index}}</td>
                      <td>{{$item->name}}</td>
                      <td>{{$item->phone??'N/A'}}</td>
                      <td>{{$item->dob}}</td>
                      <td>{{$item->age}}</td>
                      <td>{{$item->sex}}</td>
                      <?php 
                        $nationality = $item->nationality;

                        if(strpos($nationality,'/is_other') >= 0){
                            $ns = explode('/',$nationality);
                            $nationality = $ns[0];
                        }
                      ?>
                      
                      <td>{{$nationality}}</td>
                      <td style="word-break:break-all !important">{{$item->note}}</td>
                      <td align="center">
                        
                        @if ($item->status == App\Helper\Helper::DEAD['id'])
                          <span class="{{App\Helper\Helper::DEAD['color']}}">{{App\Helper\Helper::DEAD['value']}}</span>

                        @elseif($item->status == App\Helper\Helper::SUSPECTED['id'])
                          <span class="{{App\Helper\Helper::SUSPECTED['color']}}">{{App\Helper\Helper::SUSPECTED['value']}}</span>

                        @elseif($item->status == App\Helper\Helper::RECOVERED['id'])
                          <span class="{{App\Helper\Helper::RECOVERED['color']}}">{{App\Helper\Helper::RECOVERED['value']}}</span>

                        @elseif($item->status == App\Helper\Helper::POSITIVE['id'])
                          <span class="{{App\Helper\Helper::POSITIVE['color']}}">{{App\Helper\Helper::POSITIVE['value']}}</span>

                        @elseif($item->status == App\Helper\Helper::NEGATIVE['id'])
                          <span class="{{App\Helper\Helper::NEGATIVE['color']}}">{{App\Helper\Helper::NEGATIVE['value']}}</span>
                        @endif  
                      </td>
                      <td>{{$item->created_at}}</td>
                      <td align="center">
                        <a href="{{route('case.edit',$item->id)}}">
                          <i class="fas fa-edit"></i>
                        </a>
                        <a href="#" data-toggle="modal" data_id="{{$item->id}}" data-target="#modal-delete" onclick="onClickDelete('case/destroy',{{$item->id}})">
                          <i class="fas fa-trash"></i>
                        </a>
                        <a href="{{route('case.show',$item->id)}}">
                          <i class="fas fa-eye"></i>
                        </a>
                      </td>

                    </tr>
                  @endforeach
                  
                </tbody>
              </table>
            </div>
            {{-- {{$datas->appends(request()->except('page'))->links()}} --}}
          </div>
        </div>

    </div>
      <!-- /.container-fluid -->
@endsection

@section('script')
    @include('partials.js-plugin-for-table')

@endsection