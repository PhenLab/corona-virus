<script>
  var note_textarea = $('#note')

  initializeEmojiPicker('note');

  var emoji_note_input_content  = $(note_textarea).parents().find('div.twemoji-textarea')[0];
  $(emoji_note_input_content).html(twemoji.parse(note_textarea.text()))

  
  function applyNoteValue() {
      var noteText = $(note_textarea).text();
      noteText = cleanTextWithEmoji(noteText);
      $('#noteValue').val(noteText)

  }
</script>