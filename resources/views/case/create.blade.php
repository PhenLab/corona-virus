@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select_country/css/countrySelect.css')}}">
    @include('partials.emoji.css-for-twemoji')
    @include('partials.emoji.script-for-emoji')
    @include('partials.emoji.css-for-emoji-editor')

    <style>
      
      #map {
        width: 100%;
        height: 400px;
      }
    </style>
@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 mb-0 text-gray-800">Create Case</h1>
        <a href="{{route('case.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>
      </div>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Case</h6>
        </div> --}}
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class=" col-md-12">
              <form class="form-horizontal" method="POST" action="{{route('case.store')}}">
                @csrf
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="name">Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{old('name')}}" id="name" placeholder="Enter name" name="name">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                  </div>
                </div>
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="phone">Phone</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{old('phone')}}" id="phone" placeholder="Enter Phone" name="phone">
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="dob">Date of Birth</label>
                  <div class="col-sm-10">          
                    <input type="text" class="form-control datepicker" value="{{old('dob')}}"  id="dob" placeholder="Enter Date of Birth" name="dob" autocomplete="off">
                    <span class="text-danger">{{ $errors->first('dob') }}</span>

                  </div>
                </div>

                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="age">Age</label>
                  <div class="col-sm-10">          
                    <input type="number" class="form-control" value="{{old('age')}}" id="age" placeholder="Enter Age" name="age">
                    <span class="text-danger">{{ $errors->first('age') }}</span>
                  </div>
                </div>
                
                <div class="form-group row">
                  <label for="nationality" class="col-sm-2 control-label form-label-text text-md-right text-lg-right text-xl-right" >Nationality 
                    {{-- <i style="color:red;">*</i> --}}
                  </label>
                  <div class="col-sm-10">  
                    <div class="form-inline">  
                      <input name="nationality" type="text" class="form-control " id="nationality" style="width:230px" >
                      <input name="other_nationality" value="{{old( 'other_nationality', @$nationality) }}" type="text" class="form-control " id="other-nationality" style="display:none; width:230px">

                      <label class="checkbox-inline control-label" width="100%" style="padding-bottom : 3px; padding-top:3px">
                        &nbsp; &nbsp; <input type="checkbox" name="is_other_nation" class="other-nationality " value="false"  > Other Nationality
                      </label>
                    </div>
                    <span class="text-danger" >{{ $errors->first('nationality') }}</span>
                    <span class="text-danger" >{{ $errors->first('other_nationality') }}</span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="city">City</label>
                  <div class="col-sm-10">
                    <select name="city" id="city" class="form-control">
                        <option class="form-control" value="0">Select City</option>
                        @foreach ($city as $info)
                        <option value="{{ $info['id'] }}">{{ $info['name'] }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="city">Or Province</label>
                  <div class="col-sm-10">
                    <select name="province" id="province" class="form-control">
                        <option class="form-control" value="0">Select Province</option>
                        @foreach ($province as $info)
                        <option value="{{ $info['id'] }}">{{ $info['name'] }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="city">District</label>
                  <div class="col-sm-10">
                    <select name="district" id="district" class="form-control">
                        <option class="form-control" value="0">Select District</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="city">Commune</label>
                  <div class="col-sm-10">
                    <select name="commune" id="commune" class="form-control">
                        <option class="form-control" value="0">Select Commune</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="sex">Sex</label>
                  <div class="col-sm-10">    
                    <select name="sex" class="form-control" id="sex">
                      <option value="male" {{old('sex')=='male'?'selected':''}}>Male</option>
                      <option value="female" {{old('sex')=='female'?'selected':''}}>Female</option>

                    </select>      
                    <span class="text-danger">{{ $errors->first('sex') }}</span>

                  </div>
                </div>
                <input type="hidden" name="noteValue" id="noteValue" value="{{old('noteValue')}}">
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text" for="note">Note</label>
                  <div class="col-sm-10">    
                    <textarea name="note" id="note" class="form-control"  rows="4">{{old('note')}}</textarea>    
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text" for="status">Status</label>
                  <div class="col-sm-10">    
                    <select name="status" class="form-control" id="status">     
                             
                      <option value="{{App\Helper\Helper::POSITIVE['id']}}" {{old('status')==App\Helper\Helper::POSITIVE['id']?'selected':''}}>{{App\Helper\Helper::POSITIVE['value']}}</option>
                      <option value="{{App\Helper\Helper::SUSPECTED['id']}}" {{old('status')==App\Helper\Helper::SUSPECTED['id']?'selected':''}}>{{App\Helper\Helper::SUSPECTED['value']}}</option>
                      <option value="{{App\Helper\Helper::NEGATIVE['id']}}" {{old('status')==App\Helper\Helper::NEGATIVE['id']?'selected':''}}>{{App\Helper\Helper::NEGATIVE['value']}}</option>
                      <option value="{{App\Helper\Helper::RECOVERED['id']}}" {{old('status')==App\Helper\Helper::RECOVERED['id']?'selected':''}}>{{App\Helper\Helper::RECOVERED['value']}}</option>
                      <option value="{{App\Helper\Helper::DEAD['id']}}" {{old('status')==App\Helper\Helper::DEAD['id']?'selected':''}}>{{App\Helper\Helper::DEAD['value']}}</option>
                    </select>      
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text" for="location">Location</label>
                  <div class="col-sm-10">          
                    <input type="text" class="form-control" value="{{old('location')}}" id="location" placeholder="Enter Location" name="location" autocomplete="off">
                    <span class="text-danger">{{ $errors->first('location') }}</span>
                  </div>
                </div>
                <div class="form-group " style="text-align:end; padding-top:10px">
                    <input type="hidden" name="is_create_new" id="is_create_new" value="false">
                    <button type="submit" onclick="applyNoteValue()" class="btn btn-primary btn-sm pull-right" onclick="$('#is_create_new').val(true)"> Save And Create New </button>
                    <button type="submit" onclick="applyNoteValue()" class="btn btn-primary btn-sm pull-right"> Save </button>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            {{-- <div class="modal-header">
              <h5 class="modal-title">Pick Any Location</h5>

              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div> --}}
            <div class="modal-body">
              <div id="map"></div>
            </div>
            <div class="modal-footer">
              {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Pick</button> --}}
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="confirmPosition">Pick</button>

            </div>
          </div>
        </div>
      </div>
@endsection

@section('script')

    <script src="{{asset('plugins/select_country/js/countrySelect.min.js')}}"></script>
    @include('partials.script-plugin-for-map-picker')
    <script>
      
      $("#nationality").countrySelect();
      
      $('.other-nationality').change(function() {
        updateNationUIbyCheckbox($(this).is(':checked'))
          
      });

      var nationality = '<?php echo old("nationality")??"Cambodian" ?>';
      var other_nationality = '<?php echo old("other_nationality") ?>';
      var is_other_nationality = '<?php echo old("is_other_nation") ?>';
      var input_nation = $('input[name="nationality"]')
      input_nation = input_nation[0]
      var input_other_nation = $('input[name="other_nationality"]')
      input_other_nation = input_other_nation[0];


      if (is_other_nationality === 'true') {
          $("#nationality").countrySelect("setCountry", nationality);
          $('.other-nationality').prop('checked', true);
          updateNationUIbyCheckbox(true)

      }
      else{
          $(input_nation).countrySelect("setCountry", nationality);

          $('.other-nationality').prop('checked', false);
          updateNationUIbyCheckbox(false)

      }

      function updateNationUIbyCheckbox(isCheck) {
        if ( isCheck === false || isCheck === 'false'){
            $('.other-nationality').parents().siblings('.country-select').show();
            $('#other-nationality').hide();
            $('#other-nationality').prop('disabled',true);
            $('.other-nationality').val('false');

          }else{

            $('.other-nationality').parents().siblings('div.country-select').hide();
            $('#other-nationality').show();
            $('#other-nationality').prop('disabled',false);
            $('.other-nationality').val('true');

          }
      }
      var input_location = $('input[name=location]');
      $(input_location).focus(function(){
          //open bootsrap modal
          $('#myModal').modal({
            show: true
          });
      });

      // Get element references
      var confirmBtn = document.getElementById('confirmPosition');
      var onClickPositionView = document.getElementById('onClickPositionView');
      var map = document.getElementById('map');
      var default_location = {lat:11.556242, lng:104.928112};
      var input_location_val = $(input_location).val()
      if( input_location_val ){
        input_location_val = input_location_val.split('-');
        console.log(input_location_val[0]+'/'+input_location_val[1]);
        default_location.lat = input_location_val[0];
        default_location.lng = input_location_val[1];
      }

      // Initialize LocationPicker plugin
      var lp = new locationPicker(map, {
        setCurrentPosition: true, // You can omit this, defaults to true
        lat:default_location.lat,
        lng: default_location.lng
        
      }, {
        zoom: 15 // You can set any google map options here, zoom defaults to 15
      });

      // Listen to button onclick event
      confirmBtn.onclick = function () {
        // Get current location and show it in HTML
        var location = lp.getMarkerPosition();
        $(input_location).val(location.lat +'-'+ location.lng);


        onClickPositionView.innerHTML = 'The chosen location is ' + location.lat + ',' + location.lng;
      };

      // Listen to map idle event, listening to idle event more accurate than listening to ondrag event
      google.maps.event.addListener(lp.map, 'idle', function (event) {
        // Get current location and show it in HTML

        var location = lp.getMarkerPosition();


      });
      
      // Clear combobox
      $('#city').on('click',function(){
        $('#province').val(0);
      });
      $('#province').on('click',function(){
        $('#city').val(0);
      });

      // Select City
      $('#city').on('change', function(e){
        console.log(e);
        var districts_id = e.target.value;
        $.get('/query-city/' + districts_id,function(data) {
          console.log(data);
          $('#district').empty();
          $.each(data, function(index, districtsObj){
            $('#district').append('<option value="'+ districtsObj.id +'">'+ districtsObj.name +'</option>');
          })
        });
      });

      // Select province 
      $('#province').on('change', function(e){
        console.log(e);
        var districts_id = e.target.value;
        $.get('/query-province/' + districts_id,function(data) {
          console.log(data);
          $('#district').empty();
          $.each(data, function(index, districtsObj){
            $('#district').append('<option value="'+ districtsObj.id +'">'+ districtsObj.name +'</option>');
          })
        });
      });
      
      // Select district 
      $('#district').on('change', function(e){
        console.log(e);
        var districts_id = e.target.value;
        $.get('/query-district/' + districts_id,function(data) {
          console.log(data);
          $('#commune').empty();
          $.each(data, function(index, districtsObj){
            $('#commune').append('<option value="'+ districtsObj.id +'">'+ districtsObj.name +'</option>');
          })
        });
      });

      // Select commune 
      $('#commune').on('change', function(e){
        console.log(e);
        var districts_id = e.target.value;
        $.get('/query-commune/' + districts_id,function(data) {
          console.log(data);
          $('#village').empty();
          $.each(data, function(index, districtsObj){
            $('#village').append('<option value="'+ districtsObj.id +'">'+ districtsObj.name +'</option>');
          })
        });
      });

    </script>
    @include('partials.js-auto-dob-and-age')
    @include('partials.emoji.js-for-emoji')
    @include('case.js-for-emoji-editor')

@endsection