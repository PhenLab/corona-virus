@extends('layout.master')
@section('style')
  @include('partials.css-plugin-for-table')
@endsection
@section('content')  
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h4 mb-0 text-gray-800">ក្រុង និង ស្រុក List</h1>
          <div>
            <a href="{{route('krong_srok.create')}}" class="btn btn-sm btn-primary "><i class="fas fa-add"></i> Create New</a>
          </div>
        </div>
        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <td >&numero;</td>
                    <th>Province</th>
                    <th>Krong & Srok</th>
                    <th style="width:185px">Created At</th>
                    <th style="width:185px">Updated At</th>
                    <th style="text-align:center; width:100px">Actions</th>
                  </tr>
                </thead>                
                <tbody>
                @php
                    $page = 0;
                      if(Request::get('page'))
                        $page  = Request::get('page')-1;
                      
                    $index = $page*20 + 1;
                    $key = 0
                  @endphp
                  @foreach ($krong_srok as $key => $item)
                    <tr>
                      <td>{{$key + $index}}</td>
                      <td>{{$item->province->name ?? ''}}</td>
                      <td>{{$item->name}}</td>
                      <td>{{$item->created_at}}</td>
                      <td>{{$item->updated_at}}</td>
                      <td align="center">
                        <a href="{{route('krong_srok.edit',$item->id)}}">
                          <i class="fas fa-edit"></i>
                        </a>
                        <a href="#" data-toggle="modal" data_id="{{$item->id}}" data-target="#modal-delete" onclick="onClickDelete('krong_srok/destroy',{{$item->id}})">
                          <i class="fas fa-trash"></i>
                        </a>
                        <a href="{{route('krong_srok.show',$item->id)}}">
                          <i class="fas fa-eye"></i>
                        </a>
                      </td>
                    </tr>
                    @php
                        $key++;
                    @endphp
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
@endsection
@section('script')
  @include('partials.js-plugin-for-table')
@endsection