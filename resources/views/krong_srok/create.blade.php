@extends('layout.master')
@section('style')
@endsection
@section('content')
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h4 mb-0 text-gray-800">Create ក្រុង និង ស្រុក</h1>
        <a href="{{route('khan.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>
      </div>
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class=" col-md-12">
              <form class="form-horizontal" method="POST" action="{{route('krong_srok.store')}}">
                @csrf
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text" for="city">Province</label>
                  <div class="col-sm-10">
                    <select name="province" id="province" class="form-control">
                        <option class="form-control" value="0">Select Province</option>
                        @foreach ($data as $info)
                        <option value="{{ $info['id'] }}">{{ $info['name'] }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text" for="name">Krong</label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="krong" value="1" class="custom-checkbox krong"> For create krong
                    <span class="text-danger">{{ $errors->first('krong') }}</span>
                  </div>
                </div>
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text lbname" for="name">Srok</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control name" value="{{old('name')}}" id="name" placeholder="Enter Srok" name="name">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                  </div>
                </div>
                <div class="form-group " style="text-align:end; padding-top:10px">  
                  <button type="submit" class="btn btn-primary btn-sm pull-right"> Save </button>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
@endsection
@section('script')
<script>
  $(document).ready(function(){
    $(".krong").click(function(){
        if(this.checked){
          $(".lbname").text("Krong");
          $(".name").attr("placeholder","Enter Krong");
        }else{
          $(".lbname").text("Srok");
          $(".name").attr("placeholder","Enter Srok");
        }
    });
  });
</script>
@endsection