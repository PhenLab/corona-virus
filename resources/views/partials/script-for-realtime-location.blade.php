<script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{config("app.map_api_key")}}&callback=initMap">
    </script>