
<script>  
    /** 
       * @Author: Phen 
       * @Date: 2020-03-25 14:36:42 
       * @Desc: auto fill dob and age  
       */    
    var input_dob = $('input#dob')
    var input_age = $('input#age')
    
    $(input_age).on("input",function(e) {
        var age = $(e.target).val()
    
        var current_date  = new Date();
        age = Number(age)*(-1);
        var dob =  moment(current_date).add(age*12,'months').format('DD/MM/YYYY');
        console.log('dob'+ age);
        $(input_dob).datepicker('setDate',dob);


    })
    $(input_dob).change(function(e) {
        var dob = $(e.target).val()
        dob = moment(dob,'DD/MM/YYYY');
        var age = moment().diff(dob,'years');
        console.log('age'+ age);
        $(input_age).val(age);

    })
</script> 