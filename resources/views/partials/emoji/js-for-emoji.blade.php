
<script src="{{asset('vendor/twemoji/twemoji-picker.js')}}"></script>
<script>
    function initializeEmojiPicker(textarea_id = 'twemoji-picker') {
        $('#'+textarea_id).twemojiPicker({
            init: '',
            placeholder: '',
            size: '35px',
            icon: 'grinning',
            iconSize: '30px',
            height: '110px',
            width: '100%',
            category: ['smile', 'cherry-blossom', 'video-game', 'oncoming-automobile', 'symbols'],
            categorySize: '30px',
            pickerPosition: 'bottom',
            pickerHeight: '150px',
            pickerWidth: '100%'
        });
    }
</script>