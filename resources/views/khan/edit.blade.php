@extends('layout.master')
@section('style')
@endsection
@section('content')
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h4 mb-0 text-gray-800">Update ខ័ណ្ឌ</h1>
        <a href="{{route('khan.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>
      </div>
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class=" col-md-12">
            <form class="form-horizontal" method="POST" action="{{route('khan.update',$data->id)}}">
                @csrf
                @method('PUT')
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text" for="city">City</label>
                  <div class="col-sm-10">
                    <select name="city" id="city_id" class="form-control">
                        <option class="form-control" value="0">Select City</option>
                        @foreach ($cities as $info)
                        <option value="{{$info['id']}}" {{ $info['id'] == $data->city_id ? 'selected' : '' }}>{{ $info['name'] }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text" for="name">Khan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{old('name') ?? $data->name }}" id="name" placeholder="Enter Khan" name="name">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                  </div>
                </div>
                <div class="form-group " style="text-align:end; padding-top:10px">  
                  <button type="submit" class="btn btn-primary btn-sm pull-right"> Save </button>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

@endsection

@section('script')

@endsection