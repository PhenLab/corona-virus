@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select_country/css/countrySelect.css')}}">

@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h4 mb-0 text-gray-800"> Details សង្កាត់</h1>
        <div>
          <a href="{{route('sangkat.edit',@$data->id)}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-add"></i> Update</a>
          <a href="{{route('sangkat.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>

        </div>
      </div>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <dl class = "dl-horizontal">
             
              <dt>Sangkat</dt>
              <dd>{{$data->name}}</dd>
              <br/><br/>
              <dt>Created By</dt>
              <dd>{{$data->creator->name}}<br></dd>
              <br/><br/>
              <dt>Updated By</dt>
              <dd>{{$data->updater->name}}<br/></dd>
              <br/><br/>
              <dt>Created At</dt>
              <dd>{{$data->created_at}}<br></dd>
              <br/><br/>
              <dt>Updated At</dt>
              <dd>{{$data->updated_at}}<br/></dd>
{{-- 
              <dd>
                  <span class="badge badge-primary" v-for="tag in tags">{{tag}}</span><br>
              </dd>
              <br/><br/>
              <dt>Created By</dt>
              <dd>{{created_by}}<br></dd>
              <br/><br/>
              <dt>Created At</dt>
              <dd>{{formatDate(data.created_at)}}<br></dd>
              <br/><br/>
              <dt>Updated At</dt>
              <dd>{{formatDate(data.updated_at)}}<br/></dd>
              <br/><br/>
              <dt>Attachment</dt> --}}
            </dl>
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->
@endsection

@section('script')
      
@endsection