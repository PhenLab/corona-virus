@extends('layout.master')
@section('style')
@endsection
@section('content')
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h4 mb-0 text-gray-800">Create សង្កាត់</h1>
        <a href="{{route('sangkat.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>
      </div>
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class=" col-md-12">
              <form class="form-horizontal" method="POST" action="{{route('sangkat.store')}}">
                @csrf
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text" for="khan">Khan</label>
                  <div class="col-sm-10">
                    <select name="khan" id="khan" class="form-control">
                        <option class="form-control" value="0">Select Khan</option>
                        @foreach ($data as $info)
                        <option value="{{ $info['id'] }}">{{ $info['name'] }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text" for="name">Sangkat</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{old('name')}}" id="name" placeholder="Enter Sangkat" name="name">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                  </div>
                </div>
                <div class="form-group " style="text-align:end; padding-top:10px">  
                  <button type="submit" class="btn btn-primary btn-sm pull-right"> Save </button>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
@endsection
@section('script')
@endsection