@extends('layout.map-master')
@section('style')
    
  <style>
    .container{
      padding-top:15px;
      padding-bottom:15px;
      padding-right: 6px;
      padding-left: 6px;
      width: 100%;
    }
  </style>
@endsection
@section('content')
  <div class="container" >
    <div class="col-md-12" style="padding-left:0;padding-right:0">
      <form action="{{url('plot/all-cases')}}" method="get">
        <div class="col-md-10 col-sm-8 col-xs-6" style="padding-left:0">
          <select name="status" class="form-control bt-sm" id="status">
            <option value=""> ALL STATUS </option>
            <option value="{{App\Helper\Helper::POSITIVE['id']}}"  {{@$status ==App\Helper\Helper::POSITIVE['id']?'selected':''}}>  {{App\Helper\Helper::POSITIVE['value']}}</option>
            <option value="{{App\Helper\Helper::SUSPECTED['id']}}" {{@$status ==App\Helper\Helper::SUSPECTED['id']?'selected':''}}> {{App\Helper\Helper::SUSPECTED['value']}}</option>
            <option value="{{App\Helper\Helper::NEGATIVE['id']}}"  {{@$status ==App\Helper\Helper::NEGATIVE['id']?'selected':''}}>  {{App\Helper\Helper::NEGATIVE['value']}}</option>
            <option value="{{App\Helper\Helper::RECOVERED['id']}}" {{@$status ==App\Helper\Helper::RECOVERED['id']?'selected':''}}> {{App\Helper\Helper::RECOVERED['value']}}</option>
            <option value="{{App\Helper\Helper::DEAD['id']}}"      {{@$status ==App\Helper\Helper::DEAD['id']?'selected':''}}>      {{App\Helper\Helper::DEAD['value']}}</option>
          </select>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6" style="padding-right:0; padding-left:0">
          <button type="submit" class="btn btn-primary bt-sm">SEARCH</button>
          <a href="{{route('case.index')}}" class="btn btn-danger bt-sm pull-right"> Back To List</a>
        </div>

      </form>
    </div>
    
  </div>
  <div id='myDiv' style="height:700px;"><!-- Plotly chart will be drawn inside this DIV --></div>

@endsection

@section('script')
    <!-- Load plotly.js into the DOM -->
  <script src='https://cdn.plot.ly/plotly-latest.min.js'></script>

  <script>
    var storage_file_url = '{{$storage_file_url}}' 

    Plotly.d3.csv(window.location.origin + storage_file_url,
      function(err, rows) {
        function unpack(rows, key) {
          return rows.map(function(row) {
            return row[key];
          });
        }

        var data = [
          {
            type: "scattermapbox",
            text: unpack(rows, "Globvalue"),
            lon: unpack(rows, "Lon"),
            lat: unpack(rows, "Lat"),
            marker: { color: "red", size: 8 }
          }
        ];

        var layout = {
          dragmode: "zoom",
          mapbox: { style: "open-street-map", center: { lat: 11.553218291470481, lon: 104.93179312327567 }, zoom: 7 },
          margin: { r: 0, t: 0, b: 0, l: 0 }
        };

        Plotly.newPlot("myDiv", data, layout);

        var logo =  document.getElementsByClassName('cls-1');
        logo[0].style.display = 'none'
        
      }
    );
  
  </script>
@endsection