@extends('layout.master')
@section('style')
  @include('partials.emoji.css-for-twemoji')
  @include('partials.emoji.script-for-emoji')
  @include('partials.css-plugin-for-toastr')
  @include('partials.emoji.css-for-emoji-editor')

  
@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 mb-0 text-gray-800">Create Message</h1>
        <a href="{{route('news.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>
      </div>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Case</h6>
        </div> --}}
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class=" col-md-12">
              <form class="form-horizontal" method="POST" action="{{route('news.store')}}">
                @csrf

                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="query_number">Query Number</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" value="{{old('query_number')}}" id="query_number" placeholder="Enter Query Number" name="query_number">
                    <span class="text-danger">{{ $errors->first('query_number') }}</span>
                  </div>
                </div>
                {{-- <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text" for="body">Body</label>
                  <div class="col-sm-10">    
                    <textarea name="body" id="body" class="form-control"  rows="4">{{old('body')}}</textarea>
                    <span class="text-danger">{{ $errors->first('body') }}</span>    
                  </div>
                </div> --}}
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="body">Body</label>
                  <div class="col-sm-10">    
                    <textarea class="form-control textarea-emoji" rows="4" name="emoji_input"  id="twemoji-picker" ></textarea>
                    <div id="emoji-pop">
                      <div class="sp-arrow"></div>
                    </div>
                    <span class="text-danger">{{ $errors->first('body') }}</span>    
                  </div>
                </div>
                
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="description">Description</label>
                  <div class="col-sm-10">    
                    <textarea name="desc" id="description" class="form-control" rows="4">{{old('desc')}}</textarea>
                    <div id="emoji-pop">
                      <div class="sp-arrow"></div>
                    </div>
                    <span class="text-danger">{{ $errors->first('body') }}</span>    
                  </div>
                </div>
                
                <div class="form-group " style="text-align:end; padding-top:10px">
                    
                  <button type="button" class="btn btn-primary btn-sm pull-right" onclick="saveData()"> Save </button>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

@endsection

@section('script')

@include('partials.emoji.js-for-emoji')
@include('partials.js-plugin-for-toastr')

<script>


  initializeEmojiPicker();
  initializeEmojiPicker('description');

  
  function saveData(){
      var query_number = $('#query_number').val()

      var body = $('#twemoji-picker').text()
    
      body = cleanTextWithEmoji(body);
      var desc = $('#description').val();
      desc = cleanTextWithEmoji(desc);

      $.ajax({
          type: 'post',
          url : '{{route('news.store')}}',
          data:{query_number: query_number,body:body,description:desc  },

        success: function(r){

            if(r.status === 'success'){
              console.log(r.data.id)
              window.location = '/news/'+r.data.id;
            }else{
            var error = ''
              r.msg.forEach(element => {
                error += element +'\n'
              });
              toastr["error"](error)            
            }
          },
          error: function(e) {
              console.log(e);
              toastr["error"]("Creat record failed!") 


          }
      })

  }

</script>

@endsection