@extends('layout.master')
@section('style')
  @include('partials.css-plugin-for-table')

@endsection
@section('content')
    <!-- Begin Page Content -->
    
    <div class="container-fluid">
        
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h5 mb-0 text-gray-800">Message List</h1>
          <div >

            <a href="{{route('news.create')}}" class="btn btn-sm btn-primary "><i class="fas fa-add"></i> Create New</a>

          </div>
        </div>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th >&numero;</th>
                    <th style="text-align:center; width:100px">Query Number</th>
                    <th>Body</th>
                    <th>Description</th>

                    <th style="width:90px">Created By</th>
                    <th style="width:90px">Created At</th>

                    <th style="text-align:center; width:100px">Actions</th>

                  </tr>
                </thead>
                
                <tbody>
                  @php
                      $page = 0;
                      if(Request::get('page'))
                        $page  = Request::get('page')-1;
                      
                      $index = $page*20 + 1;
                  @endphp
                  @foreach (@$datas as $key => $item)
                    <tr>
                      <td>{{$key + $index}}</td>
                      <td align="center">{{$item->query_number}}</td>
                      <td style="word-break:break-all; white-space: pre-wrap; ">{!! nl2br($item->body) !!}</td>
                      <td style="word-break:break-all; white-space: pre-wrap;">{!! nl2br($item->description) !!}</td>

                      <td>{{@$item->creator->name??'N/A'}}</td>
                      <td>{{$item->created_at}}</td>

                      <td align="center">
                        <a href="{{route('news.edit',$item->id)}}">
                          <i class="fas fa-edit"></i>
                        </a>
                        <a href="#" data-toggle="modal" data_id="{{$item->id}}" data-target="#modal-delete" onclick="onClickDelete('news/destroy',{{$item->id}})">
                          <i class="fas fa-trash"></i>
                        </a>
                        <a href="{{route('news.show',$item->id)}}">
                          <i class="fas fa-eye"></i>
                        </a>
                      </td>

                    </tr>
                  @endforeach
                  
                </tbody>
              </table>
            </div>
            {{-- {{@$datas->appends(request()->except('page'))->links()}} --}}
          </div>
        </div>

    </div>
      <!-- /.container-fluid -->
@endsection

@section('script')
    @include('partials.js-plugin-for-table')
@endsection