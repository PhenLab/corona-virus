@extends('layout.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select_country/css/countrySelect.css')}}">
    @include('partials.emoji.script-for-emoji')
    @include('partials.emoji.css-for-twemoji')

@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 mb-0 text-gray-800"> Message Details </h1>
        <div>
          <a href="{{route('news.edit',@$data->id)}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-add"></i> Update</a>
          <a href="{{route('news.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>

        </div>
      </div>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Case</h6>
        </div> --}}
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Query Number</b></label>
              <div class="col-sm-10">
                <p>{{$data->query_number}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Body</b></label>
              <div class="col-sm-10">
                <p id="body" style="white-space: pre-wrap; line-height: 1.3" >{!! nl2br($data->body) !!}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Description</b></label>
              <div class="col-sm-10">
                <p id="description" style="white-space: pre-wrap; line-height: 1;" >{!! nl2br($data->description) !!}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Created By</b></label>
              <div class="col-sm-10">
                <p>{{@$data->creator->name??'N/A'}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Updated By</b></label>
              <div class="col-sm-10">
                <p>{{@$data->updater->name??'N/A'}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Created At</b></label>
              <div class="col-sm-10">
                <p>{{$data->created_at}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Updated At</b></label>
              <div class="col-sm-10">
                <p  >{{$data->updated_at}}</p>
              </div>
            </div>
            
            {{-- <dl class = "dl-horizontal">
             
              <dt>Query Number</dt>
              <dd>{{$data->query_number}}</dd>
              <br/><br/>
              <dt>Body</dt>
              <dd id="body" style="white-space: pre-wrap; line-height: 0.5;" >{!! nl2br($data->body) !!}<br></dd>
              <br/><br/>
              <dt>Description</dt>
              <dd style="white-space: pre-wrap; line-height: 1;">{!! nl2br($data->description) !!}<br></dd>
              <br/><br/>
              <dt>Created By</dt>
              <dd>{{@$data->creator->name??'N/A'}}<br></dd>
              
              <br/><br/>
              <dt>Updated By</dt>
              <dd>{{$data->updater->name}}<br/></dd>
              <br/><br/>
              <dt>Created At</dt>
              <dd>{{$data->created_at}}<br></dd>
              <br/><br/>
              <dt>Updated At</dt>
              <dd>{{$data->updated_at}}<br/></dd>

            </dl> --}}
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

@endsection

@section('script')
      <script>
        var body = $('#body')
        $(body).html(twemoji.parse($(body).html()))
        var description = $('#description')
        $(description).html(twemoji.parse($(description).html()))
      </script>
      
@endsection