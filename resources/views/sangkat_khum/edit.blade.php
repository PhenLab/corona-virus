@extends('layout.master')
@section('style')
@endsection
@section('content')
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h4 mb-0 text-gray-800">Update សង្កាត់ និង ឃុំ</h1>
        <a href="{{route('sangkat_khum.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>
      </div>
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class=" col-md-12">
            <form class="form-horizontal" method="POST" action="{{route('sangkat_khum.update',$data->id)}}">
                @csrf
                @method('PUT')
                <div class="form-group row">
                  <label class="control-label col-sm-2 form-label-text" for="krongsrok">Krong & Srok</label>
                  <div class="col-sm-10">
                    <select name="krongsrok" id="krongsrok" class="form-control">
                        <option class="form-control" value="0">Select Krong & Srok</option>
                        @foreach ($krong_srok as $info)
                        <option value="{{$info['id']}}" {{ $info['id'] == $data->krong_srok_id ? 'selected' : '' }}>{{ $info['name'] }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text" for="name">Khum</label>
                  <div class="col-sm-10">
                    <input type="checkbox" value="1" name="khum" class="custom-checkbox khum"> For create khum
                    <span class="text-danger">{{ $errors->first('khum') }}</span>
                  </div>
                </div>
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text lbname" for="name">Sangkat</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control name" value="{{old('name') ?? $data->name }}" id="name" placeholder="Enter Sangkat" name="name">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                  </div>
                </div>
                <div class="form-group " style="text-align:end; padding-top:10px">  
                  <button type="submit" class="btn btn-primary btn-sm pull-right"> Save </button>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

@endsection
@section('script')
<script>
  $(document).ready(function(){
    $(".khum").click(function(){
        if(this.checked){
          $(".lbname").text("Khum");
          $(".name").attr("placeholder","Enter Khum");
        }else{
          $(".lbname").text("Sangkat");
          $(".name").attr("placeholder","Enter Sangkat");
        }
    });
  });
</script>
@endsection